<?php

use App\Models\Propiedad;

if (! function_exists('getLastPropiedades')) {
    function getLastPropiedades(){
        $propiedades = App\Models\Propiedad::orderBy('id', 'DESC') -> paginate(5);
        return $propiedades;
    }
} 