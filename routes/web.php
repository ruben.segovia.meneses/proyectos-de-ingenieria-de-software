<?php

use Illuminate\Support\Facades\Route;
use App\Models\Categoria;
use App\Models\Propiedad;
use App\Http\Controllers\ControllerPropiedades;
use App\Http\Controllers\ContactoController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    $propiedades = Propiedad::orderBy('id','DESC')->paginate(5);
    return view('template',['propiedades'=>$propiedades]);

});
*/

Route::get('/', function () {
    //$data = Categoria::all();
    //return response()->json($data);
    return view('home');
})->name('home');
Route::get('/crearpropiedad', [ControllerPropiedades::class, 'crearPropiedad']);
Route::get('/listapropiedades', [ControllerPropiedades::class,'listPropiedades']);
Route::get('/propiedad/{slug}/detalle', [PropiedadesController::class,'detallePropiedad'])->name('detallepropiedad');
Route::post('/guardar/contacto', [ContactoContoller::class,'saveContacto'])->name('save.contacto');
